const { override, addWebpackAlias } = require('customize-cra')
const path = require('path')

module.exports = override(
    addWebpackAlias({
        ['@assets']: path.resolve(__dirname, 'src/assets'),
        ['@components']: path.resolve(__dirname, 'src/components'),
        ['@config']: path.resolve(__dirname, 'src/config'),
        ['@routes']: path.resolve(__dirname, 'src/routes'),
        ['@service']: path.resolve(__dirname, 'src/service'),
        ['@views']: path.resolve(__dirname, 'src/views'),
    })
)