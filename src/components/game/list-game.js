import React, { useState, useEffect } from 'react'
import { gameList, gameDelete } from '@service/route'
import Loader from '@components/loader/loader'
import './game.css'
import { useHistory } from 'react-router-dom'
import { Table, Button, Modal, ModalBody, ModalFooter } from 'reactstrap';


const ListGame = (props) => {

    const [games, setGames] = useState([])
    const [confirmation, setConfirmation] = useState({
        isShow: false,
        params: {}
    })
    const [isUpdate, setIsUpdate] = useState(false)
    const [loading, setloading] = useState(false)    
    const history = useHistory()

    const getList = async () => {
        try {
            const games = await gameList()
            setGames(games.data)

        } catch (err) {
            console.log('deu ruim', err)

        }

    }

    const deleteGame = async () => {

        try {

            if (confirmation.params) {
                await gameDelete(confirmation.params._id)
            }

            setConfirmation({
                isShow: false,
                params: {}
            })
            setIsUpdate(true)    


        } catch (err) {
            console.log("Nem foi", err)
        }

    }

    const editGame = (games) => {
        try {

            history.push(`editarjogo/${games._id}`)

        } catch(err) {
            console.log("Deu ruim", err)
        }

    }

    const Confirmation = () => {
        const toggle = () => setConfirmation(!confirmation.isShow)
        return(
            <Modal isOpen={confirmation.isShow} toggle={toggle} className="info">
                <ModalBody>
                    Você deseja excluir o jogo {(confirmation.params && confirmation.params.nome) || ""}
                </ModalBody>
                <ModalFooter>
                    <Button color="success" onClick={deleteGame}>SIM</Button>{' '}
                    <Button color="danger" onClick={toggle}>NÃO</Button>
                </ModalFooter>
            </Modal>
        )
    }

    const isGameEmpty = games.length === 0


    const mountTable = () => {


        const lines = games.map((games, index) => (

            <tr key={index}>
                <td>{games.nome}</td>
                <td>{games.genero}</td>
                <td>{games.distribuidora}</td>
                <td>{games.desenvolvedora}</td>
                <td>
                    <span onClick={() => editGame(games)} className="text-primary mx-1">
                        <i className="action fa fa-edit"></i>
                    </span>
                    <span onClick={() => setConfirmation({ isShow: true, params: games })} className="text-danger  mx-1">
                        <i className="action fa fa-trash"></i>
                    </span>

                </td>
            </tr>

        ))


        return !isGameEmpty ? (
            <Table className="table table-striped table-sm table-light table-hover">
                <thead className="thead-dark">
                    <tr>
                        <th>Nome</th>
                        <th>Genero</th>
                        <th>Distribuidora</th>
                        <th>Desenvolvedora</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    {lines}
                </tbody>

            </Table>

        ) : ""


    }


    const fncUseEffect = () => {
        let isCancelled = false;
        (async () => {
            try {
                setloading(true)
                const gamesAll = await getList()
                if (!isCancelled) {
                    if (gamesAll) {
                        setGames(gamesAll.data)
                    }
                    setloading(false)
                    setIsUpdate(false)
                }
            } catch (error) {
                setloading(false)
            }
        })()

        return () => { isCancelled = true }
    }

    // INIT 
    useEffect(fncUseEffect, [])

    // UPDATE 
    useEffect(fncUseEffect, [isUpdate])




    return (
        <div>
        <Confirmation />
        
        <div className="list-game">

            <Loader show={loading} />
            {mountTable()}
        </div>

        </div>
    )
}

export default ListGame
