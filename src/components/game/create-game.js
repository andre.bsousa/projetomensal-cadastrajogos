import React, { useState, useEffect } from 'react';
import { createGame, getGame, gameEdit } from '@service/route'
import Loader from '@components/loader/loader'
import Alert from '@components/alert'
import '../game/game.css'
import { useParams, useHistory } from 'react-router-dom';
import { Button, FormGroup, Label, Input } from 'reactstrap';



const CreateGame = (props) => {

    const [form, setForm] = useState({})
    const [isEdit, setIsEdit] = useState(false)
    const [isSubmit, setIsSubmit] = useState(false)
    const [alert, setAlert] = useState({})
    const { jogoId } = useParams()
    const methodGame = isEdit ? gameEdit : createGame
    const history = useHistory()

    useEffect(() => {

        const getShowGame = async () => {
            const game = await getGame(jogoId)
            setForm(game.data)
        }



        if(jogoId){
            setIsEdit(true)
            getShowGame()            
        }
    },   
    
    [jogoId])



    
    

    const handleChange = (event) => {
        setForm({
            ...form,
            [event.target.name]: event.target.value
        })
        return

    }

    const isSubmitValid = () => form.nome && form.genero && form.distribuidora && form.desenvolvedora

    const submitForm = async (event) => {

        try {
            setIsSubmit(true)
            await methodGame(form)
            setForm({
                ...form
            })
            setAlert({
                type: "success",
                msg: 'Seu formulário foi enviado com sucesso',
                show: true
            })
            setIsSubmit(false)
            setTimeout( () => history.push('/user/listarjogo'), 3000)

        } catch (err) {
            setAlert({
                type: "error",
                msg: 'Ocorreu um erro no cadastro',
                show: true
            })
            setIsSubmit(false)

        }



    }

    return (

        <React.Fragment>
            <Alert type={alert.type || ""} msg={alert.msg || ""} show={alert.show || false} />

            <div id="create_game">
                <div className="form_create">
                    <FormGroup>
                        <Label htmlFor="save_nome" className="save_nome">Nome</Label>
                        <Input type="text" id="save_nome" disabled={isSubmit} onChange={handleChange} value={form.nome || ""} name="nome" placeholder="Insira o nome do jogo">
                        </Input>
                    </FormGroup>
                    <FormGroup check>
                        <Label htmlFor="rpg" className="radio_rpg">
                        <Input type="radio" onChange={handleChange} disabled={isSubmit} name="genero" value="RPG" />
                        RPG
                        </Label>
                    </FormGroup>
                    <FormGroup check>
                        <Label htmlFor="fps" className="radio_fps">
                        <Input type="radio" name="genero" onChange={handleChange} disabled={isSubmit} value="FPS" />
                        FPS
                        </Label>
                    </FormGroup>
                    <FormGroup check>
                        <Label htmlFor="tps" className="radio_tps">
                        <Input type="radio" name="genero" onChange={handleChange} disabled={isSubmit} value="TPS" />
                        TPS
                        </Label>
                    </FormGroup>
                    <FormGroup>
                        <Label htmlFor="save_distribuidora" className="save_distribuidora">Distribuidora</Label>
                        <Input type="text" id="save_distribuidora" disabled={isSubmit} onChange={handleChange} value={form.distribuidora || ""}
                            name="distribuidora" placeholder="Insira o nome da distribuidora"></Input>
                    </FormGroup>
                    <FormGroup>
                        <Label htmlFor="save_desenvolvedora" className="save_desenvolvedora">Desenvolvedora</Label>
                        <Input type="text" id="save_desenvolvedora" disabled={isSubmit} onChange={handleChange} value={form.desenvolvedora || ""}
                            name="desenvolvedora" placeholder="Insira o nome da desenvolvedora">
                        </Input>
                    </FormGroup>
                    <Button color="primary" disabled={!isSubmitValid()} onClick={submitForm} className="create_button">{isEdit ? "Atualizar" : "Cadastrar"}</Button>

                </div>
                <Loader show={isSubmit} />
            </div>
        </React.Fragment>


    )
}

export default CreateGame;