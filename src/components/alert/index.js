import React, { useState, useEffect } from 'react'
import './style.css'

const Alert = (props) => {

    //const isShowKey = Object.keys(props).includes('show') ? props.show : true
    
    const[isShow, setShow] = useState(props.show || false)


    const closeAlert = () => {
        setShow(false)

    }

    useEffect(() => {
        setShow(props.show)
        return () => setShow(false)
    }, [props.show])


    const createAlert = (type, msg, icon, redirectMessage = false) => (

        isShow ? (

        <div className={`alert alert-${type}`}>
        <span>
            <i className={`fa fa-${icon}`}>{msg}</i>
        
        <br />
        {redirectMessage ? <small>Você sera redirecionado para a tela de Login</small> : ""}
        </span>
        <div style={{cursor: 'pointer'}} onClick={closeAlert}><i className="fa fa-close"></i></div>
            
        </div>

        ) : ""

    )

    const checkAlert = () => {
        
        switch (props.type) {
            case 'success':
                return createAlert('success', props.msg, 'check', true)     
                
            case 'error':
                return createAlert('error', props.msg, 'warning')
                      
            default:
                return createAlert('info', 'Aconteceu algo inexperado, tente novamente', 'question-circle')
                
        }
    }


    return (
        <React.Fragment>
            {checkAlert()}
        </React.Fragment>
    )
}

export default Alert
