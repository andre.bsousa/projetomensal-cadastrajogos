import React, { useState } from 'react'
import { createUser } from '@service/route'
import '../user/create-user.css'
import Alert from '@components/alert'
import { useHistory } from 'react-router-dom'
import { Button, FormGroup, Label, Input } from 'reactstrap';

const CreateUser = (props) => {

    const [form, setForm] = useState({})
    const [alert, setAlert] = useState({})
    const [isSubmit, setIsSubmit] = useState(false)

    const history = useHistory()


    const handleChange = (event) => {
        setForm({
            ...form,
            [event.target.name]: event.target.value

        })
        return
    }

    const isSubmitValid = () => form.nome && form.email && form.senha


    const submitForm = async (event) => {

        try {
            setIsSubmit(true)
            await createUser(form)
            setAlert({
                type: 'success',
                message: 'Usuario cadastrado com sucesso',
                show: true
            })
            setForm({})
            setTimeout(() => history.push('login'), 3000)

        } catch (err) {
            if(err){
                err.response.data.erros.map(item => console.log(item.msg))
                setAlert({
                    type: 'error',
                    message: 'Ocorreu um erro ao cadastrar',
                    show: false
                })
                setIsSubmit(false)
            }            

        }


    }




    return (

        <React.Fragment>
        <div id="create_user">        
        <Alert type={alert.type || ""} msg={alert.message || ""} show={alert.show || false} />
            <div className="form_create">
                <FormGroup>
                    <Label htmlFor="save_nome" className="save_nome">Nome: </Label>
                    <Input disable={isSubmit} type="text" id="save_nome" name="nome" onChange={handleChange} value={form.nome || ""} placeholder="Insira seu nome"></Input>
                </FormGroup>
                <FormGroup>
                    <Label htmlFor="save_email" className="save_email">Email: </Label>
                    <Input disable={isSubmit} tyoe="email" id="save_email" name="email" onChange={handleChange} value={form.email || ""} placeholder="Insira seu email"></Input>
                </FormGroup>
                <FormGroup>
                    <Label htmlFor="save_senha" className="save_senha">Senha: </Label>
                    <Input disable={isSubmit} type="password" id="save_senha" name="senha" onChange={handleChange} value={form.senha || ""} placeholder="Insira sua senha"></Input>
                </FormGroup>
                <Button color="primary" disabled={!isSubmitValid()} onClick={submitForm} className="create_button">Cadastrar</Button>
            </div>
            <br />
            {isSubmit ? <div>Carregando...</div> : ""}

        </div>
    </React.Fragment>

    )

}

export default CreateUser