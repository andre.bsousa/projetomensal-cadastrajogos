import React, { useState } from 'react';
import '../login/login.css';
import { useHistory } from 'react-router-dom';
import { authentication } from '@service/auth';
import { saveToken } from '@config/auth';
import { clientHttp } from '@config/config';
import { Button, FormGroup, Input, Alert } from 'reactstrap';


const Login = (props) => {

  const [form, setForm] = useState({})
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState("")
  const history = useHistory()

  const handleChange = (event) => {
    setForm({
      ...form,
      [event.target.name]: event.target.value
    })
    return

  }

  const isSubmitValid = () => form.email && form.senha

  const pressEnter = (event) => event.key === 'Enter' ? submit() : null


  const submit = async () => {
    if (isSubmitValid()) {
      setLoading(true)
      try {
      const { data: { token }} = await authentication(form)
      clientHttp.defaults.headers['x-auth-token'] = token
      saveToken(token)
      history.push('/user/listarjogo')
      } catch(err) {
        setLoading(false)
        const currentErro = err.response.data.errors
        if(currentErro){
          const allItens = currentErro.map(item => item.msg)
          const allItensToString = allItens.join('-')
          setError(allItensToString)
        }

      }

    }

    return

  }

  return (

    <React.Fragment>


      <div id="login">
        <div className="form_login">
        <div className="title">Realize seu Login</div>
          <FormGroup>
            <label htmlFor="auth_login" className="auth_login">Login: </label>
            <Input disabled={loading} type="email" id="email" name="email" onChange={handleChange} placeholder="Insira seu e-mail" value={form.email || ""} onKeyPress={pressEnter} />

          </FormGroup>
          <FormGroup>
            <label htmlFor="auth_senha" className="auth_senha">Senha: </label>
            <Input disabled={loading} type="password" id="senha" name="senha" onChange={handleChange} placeholder="Insira sua senha" value={form.senha || ""} onKeyPress={pressEnter} />
          </FormGroup>
          <Button color="primary" disabled={!isSubmitValid()} onClick={submit} >{loading ? (<i className="fa fa-spinner fa-spin fa-2x fa-fw"></i>) : "Logar" }</Button>
          <div className="alertLogin mt-2">
                        <Alert color="danger" isOpen={error || false} toggle={() => setError("")}>
                            {error}
                        </Alert>
            </div>

        </div>


      </div>
    </React.Fragment>

  )
}

export default Login