import React from 'react'
import SlideShow from '@components/layout/slide-show/slide-show'

const Home = () => {

    return (
        <div>
                <div>
                    <SlideShow />
                </div>
        </div>
    )

}

export default Home