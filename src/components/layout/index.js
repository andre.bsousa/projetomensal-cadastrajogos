import React from 'react';
import Footer from '@components/layout/footer/footer'
import Nav from '@components/layout/nav-bar/nav-bar'


const layout = (props) => {
    return (
        <div>
            <Nav {...props} />
            <main>
                {props.children}
            </main>
            <Footer />
        </div>
    )

}

export default layout;
