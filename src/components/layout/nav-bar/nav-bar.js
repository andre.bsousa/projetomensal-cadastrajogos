import React from 'react'
import './nav-bar.css'
import { useHistory } from 'react-router-dom'
import { isAuthenticated, removeToken } from '../../../config/auth'

const Nav = (props) => {

    const history = useHistory()
    const changePage = (to) => history.push(to)

    const hasUser = () => {
        if (props.info && props.info.nome) {
            return (
                <>
                    <span>{props.info.nome}</span>
                </>
            )
        }
    }

    const logout = () => {
        removeToken()
        history.push('/login')
    }


    return (
        <ul>
            <li onClick={() => changePage('/')}>Home</li>
            { isAuthenticated() ? <li onClick={() => changePage('/user/cadastrarjogo')}>Cadastrar Jogo</li> : ""}
            { isAuthenticated() ? <li onClick={() => changePage('/user/listarjogo')}>Lista de Jogos</li> : "" }
            <li>
                {isAuthenticated() ? hasUser() : <span onClick={() => changePage('login')}>Logar</span>}/
                {isAuthenticated() ? <span onClick={logout}>Logout</span> : <span onClick={() => changePage('cadastrar')}>Cadastrar</span>}
            </li>
        </ul>
    )
}

export default Nav