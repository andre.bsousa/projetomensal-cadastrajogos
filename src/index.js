import React from 'react';
import ReactDOM from 'react-dom';
import './global.css';
import Routers from '../src/routes/routes'
import 'bootstrap/dist/css/bootstrap.min.css'

ReactDOM.render(
  <React.StrictMode>
    <Routers />
  </React.StrictMode>,
  document.getElementById('root')
);