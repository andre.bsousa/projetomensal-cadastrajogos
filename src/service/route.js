import { clientHttp } from '@config/config'

const createUser = (data) => clientHttp.post(`/user`, data)
const listUser = (data) => clientHttp.get(`/user/${data._id}`)
const createGame = (data) => clientHttp.post(`/jogos`, data)
const gameDelete = (data) => clientHttp.delete(`/jogos/${data}`)
const getGame = (id) => clientHttp.get(`/jogos/${id}`)
const gameEdit = (data) => clientHttp.put(`/jogos/${data._id}`, data)

const gameList = () => clientHttp.get(`/jogos`)

export {
    createUser,
    listUser,
    gameList,
    createGame,
    gameDelete,
    getGame,
    gameEdit
}