import React from 'react'
import CreateUser from '../components/user/create-user'
import Layout from '../components/layout'


const Cadastro = () => {
    return (
        <Layout >
        <CreateUser />
        </Layout>       
    )
}

export default Cadastro
