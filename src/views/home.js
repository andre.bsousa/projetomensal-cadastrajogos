import React, { useEffect, useState } from 'react'
import Home from '../components/home/home'
import Layout from '../components/layout'
import { getToken, isAuthenticated } from '../config/auth'
import jwt from 'jsonwebtoken'


const ViewHome = () => {

    const [userInfo, setUserInfo] = useState({})

    useEffect(() => {

        ( async () => {
            if(isAuthenticated()){
            const { user } = await jwt.decode(getToken())
            setUserInfo(user)
            console.log(user)
            }
        })()
        return () => {   }
    }, [])



    return (
        <Layout info={userInfo}>
        <Home />
        </Layout>            
    )
}

export default ViewHome
