import React, { useEffect, useState } from 'react'
import { Route, Switch } from 'react-router-dom'
import Layout from '../components/layout'
import jwt from 'jsonwebtoken'
import { getToken } from '../config/auth'
import CreateGame from '../components/game/create-game'
import ListGame from '../components/game/list-game'

const User = (props) => {

    const [userInfo, setUserInfo] = useState({})

    useEffect(() => {
        ( async () => {
            const { user } = await jwt.decode(getToken())
            setUserInfo(user)
            console.log(user)
        })()
        return () => {   }
    }, [])




    return (

        <Layout info={userInfo}>
            <Switch>
            <Route exact match path = "/user/listarjogo" component={ListGame} />
            <Route exact path="/user/cadastrarjogo" component={CreateGame} />
            <Route exact path="/user/editarjogo/:jogoId" component={CreateGame} />
            </Switch>
        </Layout>
       
    )
}

export default User
