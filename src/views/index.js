import viewCadastro from '@views/cadastro'
import viewHome from '@views/home'
import viewLogin from '@views/login'
import viewUser from '@views/user'

export {
    viewCadastro,
    viewHome,
    viewLogin,
    viewUser
}