import React from 'react'
import Login from '../components/login/login'
import Layout from '../components/layout'

const ViewLogin = () => {
    return (
        <Layout>
        <Login />
        </Layout>            
    )
}

export default ViewLogin
