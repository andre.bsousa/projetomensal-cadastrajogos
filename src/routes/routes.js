import React from 'react'
import {
  Switch,
  Route, 
  Redirect,
  Router
} from 'react-router-dom'
import history from '@config/history'
import { isAuthenticated } from '@config/auth'
import { viewUser, viewLogin, viewCadastro, viewHome } from '@views'

const CustomRoute = ({...rest}) => {
  if(!isAuthenticated()){
    return <Redirect to='/login'/>
  }
  
  return <Route {...rest} />
}



const Routers = () => (
  <Router history={history}>
    <Switch>
      <Route exact path="/cadastrar" component={viewCadastro} />
      <Route exact path="/login" component={viewLogin} />
      <Route exact path="/" component={viewHome} />
      <CustomRoute path="/user" component={viewUser} />  
       { /*<Route exact path="*" component={() => (<h1> 404 | Not found </h1>)} /> */ }
    </Switch>
  </Router>
  
)

export default Routers