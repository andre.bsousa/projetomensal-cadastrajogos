import axios from 'axios'
import { getToken } from '@config/auth'

const clientHttp = axios.create({
    baseURL: process.env.REACT_APP_URL || `https://localhost:8080`
})

clientHttp.defaults.headers['Content-Type'] = 'application/json'

if(getToken()){
    clientHttp.defaults.headers['x-auth-token'] = getToken()
}

export {
    clientHttp
}